# E3372s from STICK to HiLink mode installer

### You need
  - curl, usb-modeswitch, grep, awk, sudo (not necessary, last step can fail safely)
  - balong_flash in $PATH ( https://github.com/forth32/balongflash.git )
  - atinout in $PATH ( https://github.com/beralt/atinout.git will work fine )
  - 2 files we flash (core sw update version 22.x, WebUI installer), we use these (you can probably use different balong-compatible):
    - core sw: "E3372sUpdate_22.298.03.02.965.BIN" / "E3372sUpdate_22.298.03.02.965.exe"
    - webui: "Update_WEBUI_17.100.06.00.03_Hilink_V7R2_9x25_CPIO.exe"

### Scripts handle these situations:
  - Taking the device from storage/cd-rom mode to correct flashing mode (godload)
  - Taking the device from debug mode to correct flashing mode (godload)
  - Handling special state, where 12d1:1442 ttyUSB0 provides AT-mode instead of correct godload
  - Handling, when the device is hi-link but has no UI (see part with curl POST xml to /CGI)

### Step-by-step
  - (Optional) put `70-huawei-e3.rules` and `huawei_e3.conf` in correct places for UDEV usb-modeswitch and reload-rules/restart-computer
  - Put all files into one folder, together with firmware 22.x and WebUI updates
  - Plug the E3372s-153 in stick-mode to USB, and wait for it to appear in `lsusb`
  - Run `./auto_install.sh`, observe output
  - If everything runs correctly, the modem will restart itself, script will set it up with ifconfig/dhclient, and web interface should be on address `http://192.168.8.1/`

## WARNING, this is alpha version of scripts, everything you do is on your risk, i do not take any responsibility