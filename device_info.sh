#!/bin/sh

MODEM_IP="192.168.8.1"
curl -s -X GET "http://$MODEM_IP/api/webserver/SesTokInfo" > /tmp/ses_tok.xml
COOKIE=`grep "SessionID=" /tmp/ses_tok.xml | cut -b 10-147`
TOKEN=`grep "TokInfo" /tmp/ses_tok.xml | cut -b 10-41`

curl -s -X GET "http://$MODEM_IP/api/device/information" \
-H "Cookie: $COOKIE" -H "__RequestVerificationToken: $TOKEN" \
-H "Content-Type: text/xml"

# in case of broken MTD16 (data) partition, will print out error 100002
curl -s -X GET "http://$MODEM_IP/api/dialup/profiles" \
-H "Cookie: $COOKIE" -H "__RequestVerificationToken: $TOKEN" \
-H "Content-Type: text/xml"
