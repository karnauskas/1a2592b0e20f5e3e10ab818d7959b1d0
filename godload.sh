#!/bin/bash

at() {
 timeout 1 echo "$1" | atinout - /dev/ttyUSB0 -
}
echo "[godload] startup"

at "AT"
sleep 1
at "AT"
sleep 1
at "AT^GODLOAD"

echo "[godload] waiting 10 sec"
sleep 10
