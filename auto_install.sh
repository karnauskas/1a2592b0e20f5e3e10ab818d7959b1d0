#!/bin/bash

echo () {
  bash -c "echo [E3372s stick-to-hilink] $1"
}

IS_14FE=$(lsusb | grep 12d1\:14fe | wc -l)

if [ "$IS_14FE" == "1" ] ; then
  echo "14FE (mass-storage), switching to 1506 (AT mode)"
  usb_modeswitch --verbose -J -v 0x12d1 -p 0x14fe
  sleep 5
else
  echo "14FE not found, not switching"
fi

IS_1506=$(lsusb | grep 12d1\:1506 | wc -l)
IS_1C05=$(lsusb | grep 12d1\:1c05 | wc -l)
IS_14DC=$(lsusb | grep 12d1\:14dc | wc -l)
IS_1566=$(lsusb | grep 12d1\:1566 | wc -l)
if [ "$IS_1506" == "1" ] || [ "$IS_1566" == "1" ] ; then
  echo "1506/1566 to godload"
  ./godload.sh
elif [ "$IS_1C05" == "1" ] ; then
  echo "already in godload, not switching"
elif [ "$IS_14DC" == "1" ] ; then
  echo "in hi-link mode, switching back to debug"
  sudo ifconfig usb0 up
  sudo dhclient usb0
  sleep 3
  timeout 3 curl -X POST -d @sw_debug_mode.xml http://192.168.8.1/CGI
  sleep 6
  ./godload.sh
fi

IS_1C05=$(lsusb | grep 12d1\:1c05 | wc -l)
IS_1442=$(lsusb | grep 12d1\:1442 | wc -l)
if [ "$IS_1C05" == "1" ] ; then
  balong_flash -p /dev/ttyUSB2 -gd E3372sUpdate_22.298.03.02.965.BIN
  sleep 15
elif [ "$IS_1442" == "1" ] ; then
  echo "skip, 1442 means 1st balong flash already passed"
else
  echo "not in godload mode, exiting"
  exit 1
fi

IS_1506=$(lsusb | grep 12d1\:1506 | wc -l)
IS_1442=$(lsusb | grep 12d1\:1442 | wc -l)
if [ "$IS_1506" == "1" ] && [ "$IS_1442" == "0" ] ; then
  echo "1506 to godload"
  ./godload.sh
elif [ "$IS_1442" == "1" ] ; then
  echo "skip, 1442 means we are still in godload mode"
else
  echo "not 1506 after balong_flash, something went wrong"
  exit 1
fi

IS_USB0_AT=$(timeout 5 bash -c "echo 'AT' | atinout - /dev/ttyUSB0 - | grep OK | wc -l")
IS_USB0_AT=$(timeout 5 bash -c "echo 'AT' | atinout - /dev/ttyUSB0 - | grep OK | wc -l")
if [ "$IS_USB0_AT" == "1" ] ; then
  ./godload.sh
fi

sleep 5

IS_1C05=$(lsusb | grep 12d1\:1c05 | wc -l)
IS_1442=$(lsusb | grep 12d1\:1442 | wc -l)
if [ "$IS_1C05" == "1" ] || [ "$IS_1442" == "1" ] ; then
  balong_flash -p /dev/ttyUSB0 -gd Update_WEBUI_17.100.06.00.03_Hilink_V7R2_9x25_CPIO.exe
  sleep 15
else
  echo "not 1C05 after godload, something went wrong"
  exit 1
fi

IS_1F01=$(lsusb | grep 12d1\:1f01 | wc -l)
if [ "$IS_1F01" == "1" ] ; then
  usb_modeswitch --verbose -J -v 0x12d1 -p 0x1f01
  sleep 5
else
  echo "not 1F01 after balong_flash, maybe already automatically usb-modeswitched?"
fi

IS_14DC=$(lsusb | grep 12d1\:14dc | wc -l)
if [ "$IS_14DC" == "1" ] ; then
  sudo ifconfig usb0 up
  ps aux | grep -i dhclient\ usb0 | awk '{print $2}' | xargs -L1 sudo kill -9
  sudo dhclient usb0
  ./device_info.sh
fi

exit 0
